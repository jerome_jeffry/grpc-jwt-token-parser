package service

import (
	"context"
	"log"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var (
	uname string
	role  string
)

// AuthInterceptor is a server interceptor for authentication and authorization
type AuthInterceptor struct {
	jwtManager      *JWTManager
	accessibleRoles map[string][]string
}

// NewAuthInterceptor returns a new auth interceptor
func NewAuthInterceptor(jwtManager *JWTManager, accessibleRoles map[string][]string) *AuthInterceptor {
	return &AuthInterceptor{jwtManager, accessibleRoles}
}

// Unary returns a server interceptor function to authenticate and authorize unary RPC
func (interceptor *AuthInterceptor) Unary() grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (interface{}, error) {
		log.Println("--> unary interceptor: ", info.FullMethod)

		err := interceptor.authorize(ctx, info.FullMethod)
		if err != nil {
			return nil, err
		}
		//ctx = metadata.NewContext(ctx, metadata.Pairs("Username", "uname", "Role", "role"))

		return handler(attachConntext(ctx), req)
	}
}

//attach userdetails to the context
func attachConntext(ctx context.Context) context.Context {
	UserDetails := []string{uname, role}
	return context.WithValue(ctx, "userdetails", UserDetails)
}

// func attachConntext(ctx context.Context) context.Context {
// 	return metadata.AppendToOutgoingContext(ctx, "usrname", uname, "rol", role)
// }

func (interceptor *AuthInterceptor) authorize(ctx context.Context, method string) error {
	accessibleRoles, ok := interceptor.accessibleRoles[method]
	if !ok {
		// everyone can access
		return nil
	}

	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return status.Errorf(codes.Unauthenticated, "metadata is not provided")
	}

	values := md["authorization"]
	if len(values) == 0 {
		return status.Errorf(codes.Unauthenticated, "authorization token is not provided")
	}

	accessToken := values[0]
	claims, err := interceptor.jwtManager.Verify(accessToken)
	if err != nil {
		return status.Errorf(codes.Unauthenticated, "access token is invalid: %v", err)
	}
	//taking userrole and name from the claims
	role = claims.Role
	uname = claims.Username
	for _, role := range accessibleRoles {
		if role == claims.Role {
			return nil
		}
	}

	//ctx = metadata.AppendToOutgoingContext(ctx, "claims", claims.Username, claims.Role)
	//ctx := context.WithValue(context.Background(), "values", vars{uname: claims.Username, role: claims.Role})

	return status.Error(codes.PermissionDenied, "no permission to access this RPC")
}
