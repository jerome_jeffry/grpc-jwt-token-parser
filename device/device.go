package Deviceservice

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/Jerome-0609/microservice/proto/device"
	"github.com/Jerome-0609/microservice/proto/healthcheck/google.golang.org/grpc/health/grpc_health_v1"
	"github.com/Jerome-0609/microservice/proto/user"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var isDbstarted = false
var (
	uname string
	role  string
)

type Device struct {
	serverAddr string
	UserClient user.UserClient
}

//Device constructor
func NewDevice(addr string, con *grpc.ClientConn, grpcServer func(interface{})) *Device {
	// const (
	// 	defserverAddr = "localhost:4040"
	// )
	d := Device{addr, user.NewUserClient(con)}
	grpcServer(&d)
	return &d
}

//Registering Device service to GRpc server
func (dev *Device) RegisterGrpcService(srv *grpc.Server) {
	device.RegisterDeviceServer(srv, dev)
	fmt.Print("device service started on: ")
	// Startdb()
}

//Registering Health-Check service to GRpc server
func (dev *Device) RegisterHealthService(srv *grpc.Server) {
	grpc_health_v1.RegisterHealthServer(srv, dev)
}

//Returning Device service address
func (dev *Device) GetServerAddr() string {
	return dev.serverAddr
}

//mock DB
func Startdb() {
	sleepTime := 20
	log.Println("Connecting to db")
	time.Sleep(time.Duration(sleepTime) * time.Second)
	log.Println("Database is ready!")
	isDbstarted = true

}

//attach user details to the context
func attachConntext(ctx context.Context) context.Context {
	return metadata.AppendToOutgoingContext(ctx, "usrname", uname, "rol", role)
}

//retrieving metadata from the context
func retrieveMetadata(ctx context.Context) error {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return status.Errorf(codes.Unauthenticated, "metadata is not provided")
	}
	values := md["usrname"]
	if len(values) == 0 {
		return status.Errorf(codes.Unauthenticated, "username is not parsed")
	}
	uname = values[0]

	values = md["rol"]
	if len(values) == 0 {
		return status.Errorf(codes.Unauthenticated, "role is not parsed")
	}
	role = values[0]
	return nil
}

//device microservice sends deviceid, userid, username to frontend on calling FetchData()
func (dev *Device) FetchData(ctx context.Context, req *device.Request) (res *device.Result, err error) {
	if isDbstarted == true {
		devID := req.GetDeviceId()
		if err := contextError(ctx); err != nil {
			return nil, err
		}
		err = retrieveMetadata(ctx)
		if err != nil {
			return nil, err
		}
		fmt.Println("username:", uname)
		fmt.Println("userrole:", role)

		//Device service have deviceid and userid (db/Inmemory)
		//for time being hardcoded
		userid := "133"
		inp := &user.Request{
			UserId: userid,
		}
		username := ""
		if response, err := dev.UserClient.FetchData(attachConntext(ctx), inp); err == nil {
			fmt.Println("hitting user service")
			username = response.GetUserName()
			fmt.Println("Received username:", username)
		} else {
			return nil, logError(status.Errorf(codes.Internal, "cannot fetch data: %v", err))
		}

		//returning the response
		res = &device.Result{
			DeviceId: devID,
			UserId:   userid,
			UserName: username,
		}
	}
	return res, nil
}

//Check Rpc returns the health status of Device service (Unary GRpc)
func (dev *Device) Check(ctx context.Context, req *grpc_health_v1.HealthCheckRequest) (res *grpc_health_v1.HealthCheckResponse, err error) {

	if err := contextError(ctx); err != nil {
		return nil, err
	}
	log.Println("health checking")
	if isDbstarted == true {
		log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_SERVING)
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_SERVING,
		}, nil
	} else if isDbstarted == false {
		log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_NOT_SERVING)
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_NOT_SERVING,
		}, nil
	} else {
		log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_UNKNOWN)
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_UNKNOWN,
		}, nil
	}

}

//Watch Rpc returns the streaming health status of Device service.
func (dev *Device) Watch(req *grpc_health_v1.HealthCheckRequest, stream grpc_health_v1.Health_WatchServer) error {

	var res *grpc_health_v1.HealthCheckResponse
	// service := req.GetService()
	// log.Printf("Health checking for: ", service)
	ctx := stream.Context()
	if err := contextError(ctx); err != nil {
		return err
	}
	log.Println("health checking in device service")
	for {
		if isDbstarted == true {
			log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_SERVING)
			res = &grpc_health_v1.HealthCheckResponse{
				Status: grpc_health_v1.HealthCheckResponse_SERVING,
			}
			err := stream.Send(res)
			if err != nil {
				return err
			}
			break
		} else if isDbstarted == false {
			log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_NOT_SERVING)
			res = &grpc_health_v1.HealthCheckResponse{
				Status: grpc_health_v1.HealthCheckResponse_NOT_SERVING,
			}
			err := stream.Send(res)
			if err != nil {
				return err
			}
		} else {
			log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_UNKNOWN)
			res = &grpc_health_v1.HealthCheckResponse{
				Status: grpc_health_v1.HealthCheckResponse_UNKNOWN,
			}
			err := stream.Send(res)
			if err != nil {
				return err
			}
		}

	}
	return nil

}

//checking context errors
func contextError(ctx context.Context) error {
	switch ctx.Err() {
	case context.Canceled:
		return logError(status.Error(codes.Canceled, "request is canceled"))
	case context.DeadlineExceeded:
		return logError(status.Error(codes.DeadlineExceeded, "deadline is exceeded"))
	default:
		return nil
	}
}

func logError(err error) error {
	if err != nil {
		log.Print(err)
	}
	return err
}

//starting Grpc server on 4040
// func (dev *Device) Run() {
// 	// listener, err := net.Listen("tcp", ":4040")
// 	// if err != nil {
// 	// 	panic(err)
// 	// }
// 	fmt.Println("device service started")

// 	// srv := grpc.NewServer()
// 	// device.RegisterDeviceServer(srv, dev)
// 	// reflection.Register(srv)

// 	// if e := srv.Serve(listener); e != nil {
// 	// 	panic(e)
// 	// }

// }
