package main

import (
	"context"
	"fmt"
	"log"
	"net"
	"time"

	"github.com/Jerome-0609/microservice/proto/device"

	authr "github.com/Jerome-0609/microservice/proto/auth"
	"github.com/Jerome-0609/microservice/proto/frontend"
	"github.com/Jerome-0609/microservice/service"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/reflection"
	"google.golang.org/grpc/status"
)

type Frontend struct {
	// serverAddr string
	// DeviceClient device.DeviceClient
}

var (
	uname string
	role  string
)

//Device constructor
func NewFrontend() *Frontend {
	// const (
	// 	defserverAddr = "localhost:4040"
	// )
	f := Frontend{}
	// grpcServer(&f)
	return &f
}

//attach user details to the context
func attachConntext(ctx context.Context) context.Context {
	return metadata.AppendToOutgoingContext(ctx, "usrname", uname, "rol", role)
}

//retrieving metadata from the context
func retrieveMetadata(ctx context.Context) error {
	details := ctx.Value("userdetails")
	if v, ok := details.([]string); ok {
		uname = v[0]
		role = v[1]
	} else {
		return status.Errorf(codes.Unauthenticated, "couldn't parse")
	}
	return nil
}

//Fetch RPC will call device service with device and return username and userid
func (f *Frontend) Fetch(ctx context.Context, req *frontend.Request) (res *frontend.Result, err error) {

	devID := req.GetDeviceId()
	if err := contextError(ctx); err != nil {
		return nil, err
	}
	err = retrieveMetadata(ctx)
	if err != nil {
		return nil, err
	}
	fmt.Println("username:", uname)
	fmt.Println("userrole:", role)

	inp := &device.Request{
		DeviceId: devID,
	}
	username := ""
	userid := ""
	conn, err := grpc.DialContext(attachConntext(ctx), "localhost:4040", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	cc := device.NewDeviceClient(conn)
	if response, err := cc.FetchData(attachConntext(ctx), inp); err == nil {
		fmt.Println("hitting device service")
		username = response.GetUserName()
		devID = response.GetDeviceId()
		userid = response.GetUserId()
		fmt.Println("Received data:", username, devID, userid)
	} else {
		return nil, logError(status.Errorf(codes.Internal, "cannot fetch data: %v", err))
	}

	//returning the response
	res = &frontend.Result{
		DeviceId: devID,
		UserId:   userid,
		UserName: username,
	}
	return res, nil
}

const (
	secretKey     = "secret"
	tokenDuration = 15 * time.Minute
)

//roles for accessing the GRpc api
func accessibleRoles() map[string][]string {

	const ServicePath = "/frontend.Frontend/"
	return map[string][]string{
		ServicePath + "Fetch": {"admin"},
	}
}

func seedUsers(userStore service.UserStore) error {
	err := createUser(userStore, "nxadmin", "secret", "admin")
	if err != nil {
		return err
	}
	return createUser(userStore, "nxuser", "secret", "user")
}

//creating users in In-Memory map
func createUser(userStore service.UserStore, username, password, role string) error {
	user, err := service.NewUser(username, password, role)
	if err != nil {
		return err
	}
	return userStore.Save(user)
}
func main() {
	userStore := service.NewInMemoryUserStore()
	err := seedUsers(userStore)
	if err != nil {
		log.Fatal("cannot seed users: ", err)
	}
	jwtManager := service.NewJWTManager(secretKey, tokenDuration)
	authServer := service.NewAuthServer(userStore, jwtManager)
	f := NewFrontend()
	listener, err := net.Listen("tcp", "localhost:4043")
	if err != nil {
		log.Fatal("cannot start server: ", err)
	}
	interceptor := service.NewAuthInterceptor(jwtManager, accessibleRoles())

	srv := grpc.NewServer(grpc.UnaryInterceptor(interceptor.Unary()))
	frontend.RegisterFrontendServer(srv, f)
	authr.RegisterAuthServiceServer(srv, authServer)
	reflection.Register(srv)

	log.Print("frontend service running at:", "localhost:4043")
	if e := srv.Serve(listener); e != nil {
		panic(e)
	}
}

//checking context errors
func contextError(ctx context.Context) error {
	switch ctx.Err() {
	case context.Canceled:
		return logError(status.Error(codes.Canceled, "request is canceled"))
	case context.DeadlineExceeded:
		return logError(status.Error(codes.DeadlineExceeded, "deadline is exceeded"))
	default:
		return nil
	}
}

func logError(err error) error {
	if err != nil {
		log.Print(err)
	}
	return err
}
