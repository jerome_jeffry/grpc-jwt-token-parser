# GRpc-Json Web Token parser

Implemented mock Apiserver as frontend service registered with auth server on same port, which generates and verifies Json Web Tokens.
auth server stores users in In-memory map for authorization.
Authentication and Authorization happens via GRpc Interceptors.
Once the authorized user client dials frontend service, the auth server in it generates access token for the user client.
GRpc client Interceptor will attach access token to the user client's context when it calls any GRpc Api.
GRpc server Interceptor will verify the access token of the user client from its context.
User details from the accessToken will get parsed out and will be propagated to downstream microservices with a help of a context.
	
to run frontend service get into the frontend folder and run
	->go run main.go
	this will run frontend service on localhost:4043

GRPC Health Check:

Health-Check is registered to every Service.
Implemented Check Rpc and Watch Rpc in every service to get status of respective services.
To check the status of the service:
Run the services and Under grpc-health-probe run

->go run main.go -addr=localhost:4040
->go run main.go -addr=localhost:4041
to check the health status of services running in respective addresses.
Service initial starting time is mocked, So it takes time to start. So at first, "status: NOT-SERVING" will be shown, after 100 seconds
you can recheck to make sure "status: SERVING"
you can also run
->go run main.go -addr=localhost:4040 -connect-timeout 250ms -rpc-timeout 100ms
to specify connection and rpc timeouts
This grpc health probe will be made to run as a container and in the K8s Pod specification manifest,
have to specify a livenessProbe and readinessProbe for this container.
K8s probe this container at specific time to know the status of services.

# go-microservices-updated

Implemented WithGrpcServer(), which will be reused by all microservices.

This simple code has 2 simple microservices(user service and device service) and a client that will communicate via grpc.  

Code workflow:

1)client calls Device microservice with device id to retrieve device and its user information.
2)Device microservice inturn calls User microservice with userid to retrieve username.
3)Device microservice sends back deviceid, uerid, username to the client.

Run from cmd/go-microservices/main.go with command line args.

steps:

1)go run main.go user ->To run user microservice
2)go run main.go device->To run device microservice
3)client/main.go -> To run a client




