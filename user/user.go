package Userservice

import (
	"context"
	"fmt"
	"log"
	"time"

	"github.com/Jerome-0609/microservice/proto/healthcheck/google.golang.org/grpc/health/grpc_health_v1"
	"github.com/Jerome-0609/microservice/proto/user"

	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

var isDbstarted = false

var (
	uname string
	role  string
)

type User struct {
	serverAddr string
}

//User constructor
func NewUser(addr string, grpcServer func(interface{})) *User {
	// const (
	// 	defserverAddr = "localhost:4041"
	// )
	u := User{addr}
	grpcServer(&u)
	return &u
}

//Registering User service to GRpc server
func (us *User) RegisterGrpcService(srv *grpc.Server) {
	user.RegisterUserServer(srv, us)
	fmt.Print("user service started on: ")

}

//Returning User service address
func (us *User) GetServerAddr() string {
	return us.serverAddr
}

//Registering Health-check service to GRpc server
func (us *User) RegisterHealthService(srv *grpc.Server) {
	grpc_health_v1.RegisterHealthServer(srv, us)
}

//mock DB
func Startdb() {
	sleepTime := 20
	log.Println("Connecting to db")
	time.Sleep(time.Duration(sleepTime) * time.Second)
	log.Println("Database is ready!")
	isDbstarted = true

}

//retrieving metadata from the context
func retrieveMetadata(ctx context.Context) error {
	md, ok := metadata.FromIncomingContext(ctx)
	if !ok {
		return status.Errorf(codes.Unauthenticated, "metadata is not provided")
	}
	values := md["usrname"]
	if len(values) == 0 {
		return status.Errorf(codes.Unauthenticated, "username is not parsed")
	}
	uname = values[0]

	values = md["rol"]
	if len(values) == 0 {
		return status.Errorf(codes.Unauthenticated, "role is not parsed")
	}
	role = values[0]
	return nil
}

//user microservice sends Usernames to device micoservice on calling Fetch()
func (us *User) FetchData(ctx context.Context, req *user.Request) (res *user.Result, err error) {
	if isDbstarted == true {
		userid := req.GetUserId()
		if err := contextError(ctx); err != nil {
			return nil, err
		}
		err = retrieveMetadata(ctx)
		if err != nil {
			return nil, err
		}
		fmt.Println("username:", uname)
		fmt.Println("userrole:", role)
		//user service have usernames (db/Inmemory)
		//for time being hardcoded
		fmt.Println("fetching username for userid:", userid)

		//returning the response
		res = &user.Result{
			UserName: "Netobjex",
		}
	}
	return res, nil

}

//Check Rpc returns the health status of User service (Unary GRpc)
func (us *User) Check(ctx context.Context, req *grpc_health_v1.HealthCheckRequest) (res *grpc_health_v1.HealthCheckResponse, err error) {

	if err := contextError(ctx); err != nil {
		return nil, err
	}
	log.Println("health checking in user service")
	if isDbstarted == true {
		log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_SERVING)
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_SERVING,
		}, nil
	} else if isDbstarted == false {
		log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_NOT_SERVING)
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_NOT_SERVING,
		}, nil
	} else {
		log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_UNKNOWN)
		return &grpc_health_v1.HealthCheckResponse{
			Status: grpc_health_v1.HealthCheckResponse_UNKNOWN,
		}, nil
	}

}

//Watch Rpc returns the streaming health status of User service.
func (us *User) Watch(req *grpc_health_v1.HealthCheckRequest, stream grpc_health_v1.Health_WatchServer) error {

	//checking whether service is properly up, running and responding to requests
	var res *grpc_health_v1.HealthCheckResponse
	// service := req.GetService()
	// log.Printf("Health checking for: ", service)
	ctx := stream.Context()
	if err := contextError(ctx); err != nil {
		return err
	}
	log.Println("health checking in user service")
	for {
		if isDbstarted == true {
			log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_SERVING)
			res = &grpc_health_v1.HealthCheckResponse{
				Status: grpc_health_v1.HealthCheckResponse_SERVING,
			}
			err := stream.Send(res)
			if err != nil {
				return err
			}
			break
		} else if isDbstarted == false {
			log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_NOT_SERVING)
			res = &grpc_health_v1.HealthCheckResponse{
				Status: grpc_health_v1.HealthCheckResponse_NOT_SERVING,
			}
			err := stream.Send(res)
			if err != nil {
				return err
			}
		} else {
			log.Printf("Server's status is %s", grpc_health_v1.HealthCheckResponse_UNKNOWN)
			res = &grpc_health_v1.HealthCheckResponse{
				Status: grpc_health_v1.HealthCheckResponse_UNKNOWN,
			}
			err := stream.Send(res)
			if err != nil {
				return err
			}
		}

	}
	return nil
}

//checking context errors
func contextError(ctx context.Context) error {
	switch ctx.Err() {
	case context.Canceled:
		return logError(status.Error(codes.Canceled, "request is canceled"))
	case context.DeadlineExceeded:
		return logError(status.Error(codes.DeadlineExceeded, "deadline is exceeded"))
	default:
		return nil
	}
}

func logError(err error) error {
	if err != nil {
		log.Print(err)
	}
	return err
}

//starting Grpc server on 4041
// func (us *User) Run() {

// 	// listener, err := net.Listen("tcp", ":4041")
// 	// if err != nil {
// 	// 	panic(err)
// 	// }
// 	fmt.Println("user service started")
// 	// srv := grpc.NewServer()
// 	// user.RegisterUserServer(srv, us)
// 	// reflection.Register(srv)

// 	// if e := srv.Serve(listener); e != nil {
// 	// 	panic(e)
// 	// }

// }
