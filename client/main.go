package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"time"

	// "io"
	// "log"

	client "github.com/Jerome-0609/microservice/authcl"
	"github.com/Jerome-0609/microservice/proto/frontend"
	"github.com/Jerome-0609/microservice/proto/healthcheck/google.golang.org/grpc/health/grpc_health_v1"
	"google.golang.org/grpc"
)

//health checking user service with the help of check method
func healthcheckServerCheck(ctx context.Context, conn *grpc.ClientConn) {
	hcClient := grpc_health_v1.NewHealthClient(conn)
	req := &grpc_health_v1.HealthCheckRequest{
		// Service: "device.Device",
		Service: "user.User",
	}
	if response, err := hcClient.Check(ctx, req); err == nil {
		fmt.Println("Status:", response.GetStatus())

	} else {

		if err.Error() == "deadline is exceeded" {
			//serverStatus = "Unhealthy"
			fmt.Println("server is unhealthy")
		}
		fmt.Println(err.Error())
	}
}

//health checking device service with the help of watch method(streaming GRpc)
func healthcheckServerWatch(ctx context.Context, conn *grpc.ClientConn) {
	hcClient := grpc_health_v1.NewHealthClient(conn)
	req := &grpc_health_v1.HealthCheckRequest{
		Service: "device.Device",
	}
	stream, err := hcClient.Watch(ctx, req)
	if err != nil {
		log.Fatal("cannot get status: ", err)
	}
	for {
		res, err := stream.Recv()
		if err == io.EOF {
			return
		}
		if err != nil {
			log.Fatal("cannot receive response: ", err)
		}

		status := res.GetStatus()
		log.Print("status: ", status)
	}
}

const (
	uname           = "nxadmin"
	pwd             = "secret"
	refreshDuration = 30 * time.Second
)

func authMethods() map[string]bool {

	const ServicePath = "/frontend.Frontend/"

	return map[string]bool{
		ServicePath + "Fetch": true,
	}
}
func main() {
	ctx, cancel := context.WithTimeout(context.Background(), 100*time.Second)
	defer cancel()

	conn, err := grpc.Dial("localhost:4043", grpc.WithInsecure())
	if err != nil {
		panic(err)
	}
	defer conn.Close()

	authClient := client.NewAuthClient(conn, uname, pwd)
	interceptor, err := client.NewAuthInterceptor(authClient, authMethods(), refreshDuration)
	if err != nil {
		log.Fatal("cannot create auth interceptor: ", err)
	}

	//health check User service by Check method
	//healthcheckServerCheck(ctx, conn)

	conn1, err := grpc.Dial("localhost:4043", grpc.WithInsecure(), grpc.WithUnaryInterceptor(interceptor.Unary()))
	if err != nil {
		panic(err)
	}
	defer conn1.Close()
	//health check Device service by watch method
	//healthcheckServerWatch(ctx, conn1)

	//calling Device service
	client := frontend.NewFrontendClient(conn1)
	//hardcoded for time being
	deviceid := "abcde"

	req1 := &frontend.Request{
		DeviceId: deviceid,
	}
	if response1, err := client.Fetch(ctx, req1); err == nil {
		fmt.Println("UserId:", response1.GetUserId())
		fmt.Println("UserName:", response1.GetUserName())
		fmt.Println("DeviceID", response1.GetDeviceId())
	} else {
		fmt.Println(err.Error())
	}

}
